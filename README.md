# Remote Sensing Resources

The Following are the resources that are used to create remote sensing

## Sattelite Images and Labels

### Sattelite Images Download

You need to indivisually download tiles from Sources like Mapbox or Google maps tile server:

for example to fetch image from google map server you can run

```
curl http://mt1.google.com/vt/lyrs=s&x=1325&y=3143&z=13
```

and it will download a single tile with the corresponding slippy map format (x,y,z).

Similarly download all tiles in a region and arrange in them in the following structure:

```
Images/
------z/
---------x/
------------y/
---------------tile.png
```

### Labels

Drawing labels is a long process, you need to open a GIS software like QGIS , load a map from a tileserver like google and then begin drawing features around your selected region. In QGIS you need to create a layer and then select the ADD FEATURE tool to draw features. After all the features in the region are drawn you need to export them in geojson format

## Model Training

First we need to seprate your dataset into train,validation,test in 70-20-10 fashion.

After downloading the tiles and labeling them we need to mask the features onto the tile and get a masked tile using the geojson:

An example of this is shown [here](https://medium.com/the-downlinq/creating-training-datasets-for-the-spacenet-road-detection-and-routing-challenge-6f970d413e2f)

After this you need to train the model using the steps provided by neptune AI [here](https://app.neptune.ml/neptune-ml/Mapping-Challenge/about/5-REPRODUCE_RESULTS.md)

You don't need to generate masks, since we have done that in the previous step however you need to generate the metadata. After this follow the subsequent steps of training the data and getting the trained model. Use the trained model to predict on new data and get the subsequent prediction masks. The masks are to be converted back into geojson using [this](https://www.kaggle.com/iglovikov/jaccard-polygons-polygons-mask-polygons)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Other Resources

https://github.com/neptune-ml/open-solution-mapping-challenge  
https://medium.com/the-downlinq/creating-training-datasets-for-the-spacenet-road-detection-and-routing-challenge-6f970d413e2f  
https://github.com/azavea/raster-vision  
https://github.com/agarwalt/satimage  
https://github.com/marcbelmont/satellite-image-object-detection
